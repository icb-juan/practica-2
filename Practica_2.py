# PARTE 1
# Ejercicio 1
#a)
def noEsCero(n):
    """
    Se ingresa un número entero y determina si es distinto de cero.
    """
    a=False
    if n!=0:
        a=True
    return a

help(noEsCero)

noEsCero(9)
noEsCero(0)

#b)
def iguales(n1,n2):
    """
    Toma dos números y determina si son iguales.
    """
    a=False
    if n1==n2:
        a=True
    return a

iguales(8,9)
iguales(8,8)

#c)
def menor(n1,n2):
    """
    Toma dos números y determina si el primero es menor al segundo
    """
    a=False
    if n1<n2:
        a=True
    return a

menor(8,10)
menor(9,8)

#d)
def par(n):
    """
    Toma un número y determina si es par.
    """
    a=False
    if n%2==0:
        a=True
    return a

par(8)
par(9.5)

#e)
def divisible(n,d):
    """
    Toma dos números y determina si el segundo es divisor del primero.
    """
    a=False
    if n%d==0:
        a=True
    return a

divisible(8,4)
divisible(8,3)

#f)
def imparDivisiblePorTresOCinco(n):
    """
    Toma un número y determina si es impar y múltiplo de tres y cinco. 
    """
    a=False
    if (n%3==0 or n%5==0) and n%2!=0:
        a=True
    return a

imparDivisiblePorTresOCinco(15)
imparDivisiblePorTresOCinco(30)

#Ejercicio 2
#a)
def factorial(n):
    """
    Toma un número y devuelve el factorial del mismo.
    """
    i=1
    j=1
    while i<=n:
        j=j*(i)
        i=i+1
    return j

factorial(3)
factorial(5) 

#b)
def sumaDivisores(n):
    """
    Toma un número y devuelve la suma de todos su divisores positivos
    """
    i=1
    x=0
    n=abs(n)
    while i<n:
        if n%i==0:
            x=x+i
        i=i+1
    return x

sumaDivisores(-6)
sumaDivisores(6)

#c)
def primo(n):
    """
    Toma un número y determina si es primo
    """
    i=1
    x=[]
    primalidad=True
    while i<n:
        if n%i==0:
            x.append(i)
        i=i+1
    if len(x)>=2:
        primalidad=False
    return primalidad

primo(66)
primo(67)
primo(444)
primo(953)

#d)
def menorDivisiblePorTres(n):
    """
    Toma un número positivo y devuelve el menor número mayor a n divisible
    por 3.
    """
    i=n+1
    multiplodetres=False
    while multiplodetres==False:
        if i%3==0:
            multiplodetres=True
        i=i+1
    return i-1

menorDivisiblePorTres(52)
menorDivisiblePorTres(19)

#e)
def mayorPrimo(n1,n2):
    """
    Toma dos números y determina si el primero es el mayor primo que divide al
    segundo
    """
    i=1
    x=[]
    condicion=False
    primalidad=True
    while i<n1:
        if n1%i==0:
            x.append(i)
        i=i+1
    if len(x)>=2:
        primalidad=False
    j=1
    y=[]
    n2=abs(n2)
    while j<n2:
        if n2%j==0:
            y.append(j)
        j=j+1
    if primalidad==True:
        if n1==y[len(y)-1]:
            condicion=True
    return condicion
        
mayorPrimo(11,22)
mayorPrimo(5,22)

#f)
def mcd(n1,n2):
    """
    Toma dos números y devuelve el máximo común divisor entre ellos
    """
    i=1
    x=[]
    n1=abs(n1)
    while i<n1:
        if n1%i==0:
            x.append(i)
        i=i+1
    j=1
    y=[]
    n2=abs(n2)
    while j<n2:
        if n2%j==0:
            y.append(j)
        j=j+1
    mcd=1
    k=0
    while k<len(x):
        l=0
        while l<len(y):
            if x[k]==y[l]:
                mcd=y[l]
            l=l+1
        k=k+1
    return mcd
        
mcd(30,20)
mcd(300,200)

# PARTE 2

# Ejercicio 3

#a)

def suma(a):
    """
    Toma una lista y devuelve la suma de todos sus elementos.
    """
    i=0
    x=0
    while len(a)>i:
       x=x+a[i]
       i=i+1
    return x

lista = [1,2,3,4,5,6]
suma(lista)

#b)

def promedio(a):
    """
    Toma una lista y devuelve el promedio de todos sus elementos.
    """
    i=0
    x=0
    while len(a)>i:
       x=x+a[i]
       i=i+1
    return x/len(a)

promedio(lista)
vacia=[]
# promedio(vacia)

#c)
def maximo(a):
    """
    Toma una lista  de números y devuelve el elemento de mayor valor    
    """
    i=0
    x=0
    while len(a)>i:
       if a[i]>x:
           x=a[i]
       i=i+1
    return x

lista2=[1,9,4,5]
maximo(lista2)

#d)
def listaDeAbs(a):
    """
    Toma una lista de números y devuelve los valores absolutos de cada 
    elemento de la lista
    """
    b=[]
    i=0
    while len(a)>i:
        if a[i]>=0:
            b.append(a[i])
        else:
            b.append(-a[i])
        i=i+1
    return b

list = [-1, 2, -3, 4, -5]

listaDeAbs(list)

#e)
def todosPares(a):
    """
    Toma una lista de números y devuelve True si todos los elementos de 
    la lista son pares
    """
    b=True
    i=0
    while len(a)>i:
        if a[i]%2!=0:
            b=False
        i=i+1
    return b

listapares=[2,4,6,8,10,12]

todosPares(lista2)
todosPares(listapares)

#f)

def maximoAbsoluto(a):
    """
    Toma una lista de números y devuelve el máximo entre los valores absolutos
    de todos los elementos de la lista.
    """
    x=listaDeAbs(a)
    b=maximo(x)
    return b

maximoAbsoluto(list)

#g)

def divisores(n):
    """
    Toma un número  y devuelve una lista con todos sus divisores positivos.
    """
    i=1
    x=[]
    n=abs(n)
    while i<n:
        if n%i==0:
            x.append(i)
        i=i+1
    b=suma(x)
    return b

divisores(22)

#h)

def cantidadApariciones(a, x):
    n=0
    i=0
    while i<len(a):
        if a[i]==x:
            n=n+1
            i=i+1
        else:
            i=i+1
    return n

listarep = [2,3,2,4,2,5,2,7,8,2,1,9]

cantidadApariciones(listarep, 2)    

#i)

def masRepetido(a):
    p=0
    mas=0
    k=0
    while k<len(a):
        j=cantidadApariciones(a, a[k])
        if j>p:
            p=j
            mas=a[k]
            k=k+1
        else:
            k=k+1
    return mas

masRepetido(listarep)

#j)

def ordenAscendente(a):
    outcome=True
    i=0
    while i<(len(a)-1):
        if a[i+1]<a[i]:
            outcome=False
            i=i+1
        else:
            i=i+1
            
    return outcome

ordenAscendente(listarep)
ordenAscendente(listapares)

#k)

def reverso(a):
    b=[]
    i=0
    j=(len(a)-1)
    while i<len(a):
        b.append(a[j])
        i=i+1
        j=j-1
    return b

reverso(listapares)
reverso(reverso(listapares))

## Ejercicio 4

#a)

def raizcuadrada(n):
    x=n**(1/2)
    return x

raizcuadrada(81)

#b)


def sumaposicionespares(a):
    x=0
    i=0
    while i<len(a):
        if par(i)==True:
            x=x+a[i]
            i=i+1
        else:
            i=i+1
    return x

sumaposicionespares(listapares)
sumaposicionespares(listarep)            

#c)

def capicua(a):
    b=True
    i=0
    while i<len(a):
        if a[i]!=a[len(a)-1-i]:
            b=False
            i=i+1
        else:
            i=i+1
    return b

listacapicua=[1,2,3,3,2,1]

capicua(listacapicua)
capicua(listapares)

#d)

def pseudopromedioposicionesimpares(a):
    promedio=0
    i=0
    while i<len(a):
        if i%2==1:
            promedio=promedio+a[i]
            i=i+1
        else:
            i=i+1
    return promedio/(len(a)/2)

pseudopromedioposicionesimpares(listacapicua)

#e)

def minimovalorlista(a):
    min=a[0]
    i=1
    while i<len(a):
        if a[i]<min:
            min=a[i]
            i=i+1
        else:
            i=i+1
    return min

minimovalorlista(listarep)

#f)

def longmaximaracha(a):
    rachamax=0
    i=1
    racha=1
    while i<len(a):
        if a[i]==a[i-1]:
            racha=racha+1
            if racha>rachamax:
                rachamax=racha
            i=i+1
        else:
            racha=1
            i=i+1
    return rachamax

listaracha=[1,2,3,3,3,3,3,3,3,4,5,6,6,6,6,7,8,8,8]

longmaximaracha(listaracha)

